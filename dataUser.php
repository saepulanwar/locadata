<style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    margin-bottom: 10px;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }
  .table-title{
    padding-top:10px;
  }

  .info a {
    color: #ffff; 
    } /* CSS link color */

</style>
<?php
ob_start();

if(!isset($_SESSION['akun_id'])) header("location: login.php");
include "config.php";

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "locadata";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
if($_SESSION['akun_level']=='admin'){

$sql = "SELECT * FROM user";
}else{
    $user=$_SESSION['akun_username'];
    $sql = "SELECT * FROM user WHERE username='$user'";
}


$result = $conn->query($sql);




?>

<div class="row">
    <div class="col-md-12">
        <div class="container-fluid" style="background-color: white; border-radius: 10px;">
            <p class="table-title">Data User</p>
            <table>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Username</th>
                    <!-- <th>Email</th> -->
                    <th>Password</th>
                    <th>Level</th>
                    <th>Foto</th>
                    <th scope="col">*</th>

                </tr>
                <?php
                $num = 1;
                  if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                ?>
                <tr>
                    <td><?php echo $num++;?></td>
                    <td><?php echo $row['nama'];?></td>
                    <td><?php echo $row['username'];?></td>
                    <!-- <td>?php echo $row['email'];?></td> -->
                    <td><?php echo $row['password'];?></td>
                    <td><?php echo $row['level'];?></td>
                    <td><img src="images/<?php echo ($row['foto']);?>" width="60" height="80"></td>
                    <?php 
                        // if($_SESSION['akun_level']=='admin'){
                        echo 
                        "<td align='center'>
                        <a id='edit_user' data-toggle='modal' data-target='#edit-user' data-id_user='".$row['id_user']."'"."data-nama2='".$row['nama']."'"."data-username2='".$row['username']."'"."data-password='".$row['password']."'"."data-level='".$row['level']."'".">
                            <button style='border-radius:8px;' class='btn btn-primary btn-xs'><i class='fa fa-edit'></i>Edit</button>
                        </a>
                        <a id='hapus_user' data-toggle='modal' data-target='#hapus-user' data-id_user='".$row['id_user']."'"."data-nama2='".$row['nama']."'"."data-username2='".$row['username']."'"."data-password='".$row['password']."'"."data-level='".$row['level']."'".">
                            <button style='border-radius:8px;' class='btn btn-danger btn-xs'><i class='fa fa-delete'></i>Hapus</button>
                        </a>
                        </td>";
                    // }
                    ?>
                </tr>
                <?php
                    }
                  } else {
                    echo "0 results";
                  }
                  // $conn->close();
                ?>
            </table>                        
        </div>
    </div>
</div>

<!-- modal hapus user -->
<div class="modal" id="hapus-user" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <form id="form_hapus_user" enctype="multipart/form-data">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Hapus User?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="modal-hapus">
            <div class="form-group">
                <p>Yakin hapus user dengan nama <span id="nama2"></span>?</p>
                <!-- <input type="text" id="nama" name="nama"> -->
                <input  type="hidden" id="id_user" name="id_user">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
            <input type="submit" class="btn btn-primary" name="submit" value="Hapus">
        </div>
        </div>
    </form>
  </div>
</div>

<!-- Modal pop up edit user -->
<div class="modal fade" id="edit-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Form Perubahan Data User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form_edit_user" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
        
            <div class="form-group">
                <label for="nama2">Nama</label>
                <input type="text" name="nama2" class="form-control" id="nama2" required>
                <input  type="hidden" id="id_user" name="id_user">
            </div>
            <div class="form-group">
                <label for="username2">username</label>
                <input type="text" name="username2" class="form-control" id="username2" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="text" name="password" class="form-control" id="password" required>
            </div>
            <?php if($_SESSION['akun_level'] == 'admin'){?>
                <div class="form-group">
                    <label for="level">Level</label>
                    <select class="dropdown form-control" id = "level" name="level" required>
                        <option value = "admin">Admin</option>
                        <option value = "umum">Umum</option>
                    </select>
                </div>
            <?php }?>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
        </div>
        </form>
    </div>
    </div>
</div>

<!-- pop up tambah user -->
<div class="modal fade" id="tambah-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Form Tambah User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form-tambah" name="form" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" id="nama" name="nama" class="form-control" required>
                <div id="feedback2">here</div>
            </div>
            <div class="form-group">
                <label for="nik">NIK</label>
                <input type="number" name="nik" class="form-control" id="nik" required>
                <div id="feedback3"></div>
            </div>
            <div class="form-group">
                <label for="username">username</label>
                <input type="text" name="username" class="form-control" id="username" required>
                <div id="feedback"></div>
            </div>
            <!-- <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" id="email" required>
            </div> -->
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" id="password" required>
                <input type="checkbox" class="form-checkbox"> Show password
            </div>
            <div class="form-group">
                <label for="level">Level</label>
                <select class="dropdown form-control" id = "level" name="level">
                    <option value = "admin">Admin</option>
                    <option value = "umum">Umum</option>
                </select>
            </div>
            <div class="form-group">
                <label for="foto">Foto</label>
                <input type="file" name="foto" class="form-control" id="foto" required>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
            <input id="feedback4" type="submit" class="btn btn-primary" name="submit" value="Simpan">
        </div>
        </form>
    </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script type="text/javascript">

// EDIT USER
$(document).on("click", "#edit_user", function() {
    var id = $(this).data('id_user');
    var nama2 = $(this).data('nama2');
    var username2 = $(this).data('username2');
    var level = $(this).data('level');
    var password = $(this).data('password');
    $("#modal-edit #id_user").val(id);
    $("#modal-edit #nama2").val(nama2);
    $("#modal-edit #username2").val(username2);
    $("#modal-edit #level").val(level);
    $("#modal-edit #password").val(password);
})

$(document).ready(function(e) {
    $("#form_edit_user").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'edit_user.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=dataUser";
    }));
});

// HAPUS USER
$(document).on("click", "#hapus_user", function() {
    var id = $(this).data('id_user');
    var nama = $(this).data('nama2');
    var username = $(this).data('username2');
    $("#modal-hapus #id_user").val(id);
    $("#modal-hapus #nama2").text(nama);
    $("#modal-hapus #username2").val(username);

})

$(document).ready(function(e) {
    $("#form_hapus_user").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'hapus_user.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=dataUser";
    }));
});

// CHECK

$(document).ready(function(){
    $('#feedback').load('check.php').show();
    $('#feedback2').load('check2.php').show();
    $('#feedback3').load('check3.php').show();
    $('#username').keyup(function(){
        $.post('check.php', {username: form.username.value}, 
        function(result){
            $('#feedback').html(result).show();
        });
    });
    $('#nama').keyup(function(){
        $.post('check2.php', {nama: form.nama.value}, 
        function(result){
            $('#feedback2').html(result).show();
        });
    });
    $('#nik').keyup(function(){
        $.post('check3.php', {nik: form.nik.value}, 
        function(result){
            $('#feedback3').html(result).show();
        });
    });
});
// penilaian

$(document).ready(function(e) {
    $("#form-tambah").on("submit", (function(e) {
    e.preventDefault();
    // alert('sad');
    $.ajax({
        url:'tambah_user.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=dataUser";
    }));
    
});

// show pass

$(document).ready(function(){		
		$('#modal-edit .form-checkbox').click(function(){
			if($(this).is(':checked')){
				$('#modal-edit #password').attr('type','text');
			}else{
				$('#modal-edit #password').attr('type','password');
			}
		});
	});

</script>