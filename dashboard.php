<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<?php  
ob_start();

if(!isset($_SESSION['akun_id'])) header("location: login.php");
include "config.php";

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "locadata";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "SELECT 
	COUNT(IF(jenis_kelamin='laki-laki',1,null)) AS L,
    COUNT(IF(jenis_kelamin='perempuan',1,null)) AS P,
    COUNT(jenis_kelamin) AS total,
    COUNT(DISTINCT(no_kk)) AS kk
    FROM data_penduduk";
    
    $result = $conn->query($sql);

    // $sql2="SELECT 
	// COUNT(IF(status='sakit',1,null)) AS sakit,
    // COUNT(IF(status='sehat',1,null)) AS sehat
    // FROM laporan";
    $sql2="SELECT COUNT(status) AS jumlah, status FROM laporan GROUP BY status";
    $result2 = $conn->query($sql2);

    foreach($result as $key=>$value){
        $laki=$value['L'];
        $perempuan=$value['P'];
        $total=$value['total'];
        $kk=$value['kk'];
    }

    // foreach($result2 as $key=>$value){
    //     $sehat=$value['sehat'];
    //     $sakit=$value['sakit'];
    // }

    $sql3="SELECT COUNT(IF((SELECT YEAR(CURDATE()) - YEAR(tgl_lahir))<5,1,null)) AS balita, 
    COUNT(IF((SELECT YEAR(CURDATE()) - YEAR(tgl_lahir)) BETWEEN 5 AND 11,1,null)) AS anak, 
    COUNT(IF((SELECT YEAR(CURDATE()) - YEAR(tgl_lahir)) BETWEEN 12 AND 25,1,null)) AS remaja, 
    COUNT(IF((SELECT YEAR(CURDATE()) - YEAR(tgl_lahir)) BETWEEN 26 AND 45,1,null)) AS dewasa, 
    COUNT(IF((SELECT YEAR(CURDATE()) - YEAR(tgl_lahir)) BETWEEN 46 AND 65,1,null)) AS lansia,
    COUNT(IF((SELECT YEAR(CURDATE()) - YEAR(tgl_lahir))>65,1,null)) AS manula
    FROM data_penduduk";
    $result3 = $conn->query($sql3);
    foreach($result3 as $key=>$value){
        $balita=$value['balita'];
        $anak=$value['anak'];
        $remaja=$value['remaja'];
        $dewasa=$value['dewasa'];
        $lansia=$value['lansia'];
        $manula=$value['manula'];
    }

    $sql4="SELECT COUNT(pekerjaan) AS jumlah, pekerjaan FROM data_penduduk GROUP BY pekerjaan";
    $result4 = $conn->query($sql4);


    ?>
<div class="row">
    <div class="col-md-6">
        <div class="container-fluid">
            <div class="container-fluid" style="background-color: white; border-radius: 10px;">
                <figure class="highcharts-figure">
                    <div id="container"></div>
                </figure>               
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="container-fluid">
            <div class="container-fluid" style="background-color: white; border-radius: 10px;">
                <figure class="highcharts-figure">
                    <div id="kelompok_umur"></div>
                </figure>               
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="container-fluid">
            <div class="container-fluid" style="background-color: white; border-radius: 10px;">
                <figure class="highcharts-figure">
                    <div id="data_pekerjaan"></div>
                </figure>               
            </div>
        </div>
    </div>

    <div class="col-md-6" style="margin-top: 10px;">
        <div class="container-fluid">
            <div class="container-fluid" style="background-color: white; border-radius: 10px;">
                <figure class="highcharts-figure">
                    <div id="container2"></div>
                </figure>               
            </div>
        </div>
    </div>
</div>
<script>
  var d = new Date();
  var n = d.getFullYear();
    // Create the chart
    Highcharts.chart('data_pekerjaan', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Data Pekerjaan Penduduk'+n
    },
    // subtitle: {
    //     text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    // },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Jumlah'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.0f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Jumlah {point.name}</span>: <b>{point.y:.0f}</b><br/>',
    },

    series: [
        {
            name: "Penduduk",
            colorByPoint: true,
            data: [
                    <?php 
                    foreach($result4 as $key=>$value){
                     echo "{name: '".$value['pekerjaan']."',y:".$value['jumlah'].",},";   
                    }
                    ?>

            ]
        }
    ]
});



Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Data penduduk '+n
    },
    // subtitle: {
    //     text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    // },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Jumlah'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.0f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Jumlah {point.name}</span>: <b>{point.y:.0f}</b><br/>',
    },

    series: [
        {
            name: "Penduduk",
            colorByPoint: true,
            data: [
                {
                    name: "laki-laki",
                    y: <?php echo $laki; ?>,
                },
                {
                    name: "Perempuan",
                    y: <?php echo $perempuan; ?>,
                },
                {
                    name: "Kepala Keluarga",
                    y: <?php echo $kk; ?>,
                },
                {
                    name: "Penduduk",
                    y: <?php echo $total; ?>,
                }
            ]
        }
    ]
});

Highcharts.chart('container2', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Data Status Kesehatan Warga'
    },
    tooltip: {
        pointFormat: 'Jumlah: <b>{point.y} orang</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Persentase',
        colorByPoint: true,
        data: [
            <?php foreach($result2 as $key=>$value){
                echo "            {
                    name: '".$value['status']."',
                    y:".$value['jumlah'].",
                },";
            }?>
]
    }]
});

// KELOMPOK UMUR
Highcharts.chart('kelompok_umur', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Data Kelompok Umur '+n
    },
    tooltip: {
        pointFormat: '<b>Jumlah</b>: {point.y} orang'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Jumlah',
        colorByPoint: true,
        data: [{
            name: 'Balita',
            y: <?php echo $balita;?>,
            // sliced: true,
            // selected: true
        },
        {
            name: 'Anak-anak',
            y: <?php echo $anak;?>,
            // sliced: true,
            // selected: true
        },
        {
            name: 'Remaja',
            y: <?php echo $remaja ?>,
        },
        {
            name: 'Dewasa',
            y: <?php echo $dewasa;?>,
            // sliced: true,
            // selected: true
        },
        {
            name: 'Lansia',
            y: <?php echo $lansia;?>,
            // sliced: true,
            // selected: true
        },
        {
            name: 'Manula',
            y: <?php echo $manula;?>,
            // sliced: true,
            // selected: true
        }]
    }]
});
</script>
