<?php
ob_start();
session_start();
if(!isset($_SESSION['akun_id'])) header("location: login.php");
include "config.php";
$nama_username=$_SESSION['akun_username'];
$sql2="SELECT *,COUNT(IF(status='ditanggapi',1,null)) AS jumlah FROM keluhan_saran WHERE username_input='$nama_username'";
$result2 = $conn->query($sql2);
$result2;
foreach($result2 as $key=>$value){
  $array_username[]=$value['username_tanggapan'];
}
$check_num2 = mysqli_num_rows($result2);
// $array_username=array('anwar','edi');
// var_dump($array_username);
if($check_num2>0){
  $tes=implode("'".",'", $array_username);
  // if(is_array($tes)==true){
    // $tes2=$tes;
    $sql= "SELECT * FROM keluhan_saran, user WHERE username_input='$nama_username' AND username IN (SELECT username_tanggapan FROM keluhan_saran WHERE username_input='$nama_username' GROUP BY username_tanggapan) AND status='ditanggapi' GROUP BY id_keluhan ORDER BY tanggal DESC";
    $result = $conn->query($sql);
    $result;
  // }else{
  //   $sql= "SELECT * FROM keluhan_saran, user WHERE username_input='$nama_username' AND username='$tes' AND status='ditanggapi' ORDER BY tanggal DESC";
  //   $result = $conn->query($sql);
  //   $result;
  // }
}
$check_num = mysqli_num_rows($result);

// $sql2="SELECT * FROM keluhan_saran WHERE username_input='$nama_username'";

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>LOCADATA - Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-white sidebar sidebar-light accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">LOCADATA</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="/ta">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Home</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Pages
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Data</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="?page=dataPenduduk">Data Penduduk</a>
            <a class="collapse-item" href="?page=dataStatus">Data Kondisi</a>
            <!-- <a class="collapse-item" href="?page=dataInformasi">Data Informasi</a> -->
            <?php if($_SESSION['akun_level'] == 'admin'){?>
            <a class="collapse-item" href="?page=dataUser">Data User</a>
            <?php }?>
          </div>
        </div>
      </li>

      <!-- Nav Item - Charts -->
      <li class="nav-item">
        <a class="nav-link" href="?page=dashboard">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Dashboard</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?page=informasiDesa">
          <i class="fas fa-info-circle"></i>
          <span>Informasi Desa</span></a>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="?page=berita">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Berita</span></a>
      </li> -->

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Heading -->
      <div class="sidebar-heading">
        Formulir
      </div>

      <!-- Nav Item - lapor kondisi -->
      <li class="nav-item">
        <a class="nav-link" href="?page=laporKondisi">
          <i class="fas fa-file-medical"></i>
          <span>Lapor Kondisi</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="?page=tulisArtikel">
          <i class="fas fa-edit"></i>
          <span>Tulis Artikel</span></a>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="?page=keluhanDanSaran">
          <i class="fas fa-comment-alt"></i>
          <span>Keluhan dan saran</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <!-- <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form> -->

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
              <?php if($_SESSION['akun_level']=='umum'){?>
                <!-- Nav Item - Messages -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i>
                <!-- Counter - Messages -->
                <span class="badge badge-danger badge-counter"><?php foreach($result2 as $key=>$value){echo $value['jumlah'];}?></span>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Tanggapan Terhadap Keluhan dan Saran Anda
                </h6>
                
                        <?php 
                          foreach($result as $key=>$value){
                            echo "
                            <a class='dropdown-item d-flex align-items-center' id='message_tanggapan'  href='?page=tanggapanKeluhan' data-target='#message-tanggapan' data-id_keluhan='".$value['id_keluhan']."'"." data-id_tanggapan='".$value['tanggapan']."'".">
                              <div class='dropdown-list-image mr-3'>
                                <img class='rounded-circle' src='images/".$value['foto']."' alt=''>
                                <div class='status-indicator bg-success'></div>
                              </div>
                              <div>
                                <div class='text-truncate'>".$value['tanggapan']."
                                </div>
                                <div class='small text-gray-500'>".$value['username_tanggapan']."
                                </div>
                              </div>
                            </a>
                            <a id='tanggap_keluhan' data-toggle='modal' data-target='#tanggap-keluhan' data-id_keluhan='".$value['id_keluhan']."'"." data-user_tanggapan='".$_SESSION['akun_username']."'".">
                            </a>";
                            
                          }  
                          if($check_num == 0){
                            echo "                            <a class='dropdown-item d-flex align-items-center' id='message_tanggapan'  href='?page=tanggapanKeluhan' data-target='#message-tanggapan' data-id_keluhan='".$value['id_keluhan']."'"." data-id_tanggapan='".$value['tanggapan']."'".">
                            <div>
                              <div class='text-truncate'>No unread message!
                              </div>
                              <div class='small text-gray-500'>Sistem
                              </div>
                            </div>
                          </a>";
                          }else{
                            
                          }          
                        ?>
                
                <!-- <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a> -->
              </div>
            </li>
                        <?php }?>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['akun_nama'];?></span>
                <img class="img-profile rounded-circle" src="images/<?php echo $_SESSION['akun_foto'];?>">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <!-- <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a> -->
                <a class="dropdown-item" href="?page=dataUser">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Akun Saya
                </a>
                <!-- <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a> -->
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="logout.php">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">
            
            <?php
          $page = @$_GET['page'];
          if($page == ""){
            echo "Home <button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i><a href='?page=tulisArtikel' style='color:white'>Tulis Pengumuman</a></button>";
          }else if($page == "dataPenduduk"){
            echo "Data Penduduk  <a id='tambah_data' data-toggle='modal' data-target='#tambah-data'><button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah Data</button></a>";
          }else if($page == "dataStatus"){
            echo "Data Kondisi Kesehatan Penduduk";
          }
          // else if($page == "dataInformasi"){
          //   echo "Data Informasi";
          // }
          else if($page == "dataUser"){
            echo "Data User"." ";
            if($_SESSION['akun_level']=='admin'){
              echo "<a id='tambah_user' data-toggle='modal' data-target='#tambah-user'><button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah User</button></a>";
            }
          }
          else if($page == "dashboard"){
            echo "Dashboard";
          }
          else if($page == "laporKondisi"){
            echo "Lapor Kondisi";
          }
          else if($page == "tulisArtikel"){
            echo "Form Artikel/Berita";
          }
          else if($page == "keluhanDanSaran"){
            if($_SESSION['akun_level'] == 'admin'){
              echo "Keluhan dan Saran Warga";
            }else{
            echo "Form Keluhan dan Saran";
            }
          }
          else if($page == "berita"){
            echo "Berita Terkini";
          }
          else if($page == "setting"){
            echo "Data Akun";
          }
          else if($page == "tanggapanKeluhan"){
            echo "Tanggapan Keluhan dan Saran";
          }
          else if($page == "informasiDesa"){
            echo "Informasi Desa";
          }

        ?>
            </h1>
            <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
          </div>
          <?php
            $page = @$_GET['page'];
            if($page == ""){
              include "home.php";
            }else if($page == "dataPenduduk"){
              include "dataPenduduk.php";
            }else if($page == "dataStatus"){
              include "dataStatus.php";
            }
            // else if($page == "dataInformasi"){
            //   include "dataInformasi.php";
            // }
            else if($page == "dataUser"){
              include "dataUser.php";
            }
            else if($page == "dashboard"){
              include "dashboard.php";
            }
            else if($page == "laporKondisi"){
              include "laporKondisi.php";
            }
            else if($page == "tulisArtikel"){
              include "tulisArtikel.php";
            }
            else if($page == "keluhanDanSaran"){
              include "keluhanDanSaran.php";
            }
            else if($page == "berita"){
              include "berita.php";
            }
            else if($page == "setting"){
              include "setting.php";
            }
            else if($page == "tanggapanKeluhan"){
              include "tanggapan.php";
            }
            else if($page == "informasiDesa"){
              include "informasiDesa.php";
            }
          ?>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <small>Rancangan tugas akhir</small>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

<!-- TANGGAPAN -->
<div  style="width:100%;"class="modal fade" id="tanggap-keluhan" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalScrollableTitle">Form Tanggapan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form id="form_tanggapan" enctype="multipart/form-data">
                <div class="modal-body" id="modal-edit">
                    <label for="tanggapan" style="color:#fff; font-weight:bold;">Tanggapan :</label>
                    <input class="form-control" type="text" name="tanggapan" id="tanggapan" placeholder="Masukan tanggapan anda...">
                    <!-- <br> -->
                    <input type="hidden" id="id_keluhan" name="id_keluhan">
                    <input type="hidden" id="user_tanggapan" name="user_tanggapan">
                    <br/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
                    <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
                </div>
            </form>
        </div>
        </div>
    </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>

  <script type="text/javascript">

  $(document).ready(function(){
      $('#mytable td.status').each(function(){
          if ($(this).text() == 'sehat') {
              $(this).css('background-color','#53FCBA');
          }else if ($(this).text() == 'sakit') {
              $(this).css('background-color','#FE776D');
          }
      });
  });

// KELUHAN
    $(document).on("click", "#message_tanggapan", function() {
      
        var id_keluhan = $(this).data('id_keluhan');
        var user_tanggapan = $(this).data('user_tanggapan');
        // $("#modal-edit #id_keluhan").val(id_keluhan);
        // $("#modal-edit #user_tanggapan").val(user_tanggapan);
        // $("#modal-edit #tanggapan").val(tanggapan);
        // alert(id_keluhan);
        e.preventDefault();
        $.ajax({
            url:'tanggapan.php',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(msg) {
            $('.table').html(msg);
            }
        });
        window.location="?page=keluhanDanSaran";
    })

    $(document).ready(function(e) {
        $("#form_tanggapan").on("submit", (function(e) {
        e.preventDefault();
        $.ajax({
            url:'input_tanggapan.php',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(msg) {
            $('.table').html(msg);
            }
        });
        window.location="?page=keluhanDanSaran";
        }));
    });

  </script>
</body>

</html>
