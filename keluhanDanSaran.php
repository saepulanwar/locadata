<?php
    include('koneksi.php');

    $sql= "SELECT * FROM keluhan_saran ORDER BY tanggal DESC";
    $result = $conn->query($sql);
    $result;

    if(isset($_POST['submit'])){
        if(isset($_POST['keluhan']) && !empty($_POST['keluhan'])){
            $keluhan = $_POST['keluhan'];
            $saran=$_POST['saran'];
            $tanggal=date("Y-m-d h:i:s");
            $pengirim =$_SESSION['akun_nama'];
            $username=$_SESSION['akun_username'];
        }else{
            $empty_error = '<b class="text-danger text-center>Harap tulis artikel</b>"';

        }
        if(isset($keluhan) && !empty($keluhan)){
            $insert_q = "INSERT INTO keluhan_saran (keluhan, saran, tanggal, pengirim, username_input) values('$keluhan', '$saran', '$tanggal', '$pengirim', '$username')";

            if(mysqli_query($connection,$insert_q)){

            }else{
                $submit_error = '<b class="text-danger text-center>You are not able to submit</b>"';
            }
        }
    }
?>
<style>
  /* Style inputs, select elements and textareas */
  input[type=text], select, textarea{
    width: 100%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
  }

  /* Style the label to display next to the inputs */
  label {
    padding: 12px 12px 12px 0;
    display: inline-block;
  }

  /* Style the submit button */
  .submit {
    background-color: #4CAF50;
    color: white;
    padding: 12px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    float: right;
  }

  /* Style the container */
  .container {
    border-radius: 5px;
    background-color: #ffff;
    padding: 20px;
  }

  /* Floating column for labels: 25% width */
  .col-25 {
    float: left;
    width: 25%;
    margin-top: 6px;
  }

  /* Floating column for inputs: 75% width */
  .col-75 {
    float: left;
    width: 75%;
    margin-top: 6px;
  }

  /* Clear floats after the columns */
  .row:after {
    content: "";
    display: table;
    clear: both;
  }

  /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
  @media screen and (max-width: 600px) {
    .col-25, .col-75, input[type=submit] {
      width: 100%;
      margin-top: 0;
    }
  }
</style>

<?php if($_SESSION['akun_level'] == 'admin'){?>
<div class="row">
<?php
foreach($result as $key=>$value){?>
    <div class="col-md-6">
        <div class="container-fluid" style="background-color:#00FB93; margin:5px;padding:5px; border-radius:20px;">
            <div>
                <p>Tanggal laporan : <?php echo $value['tanggal'];?></p>
            </div>
            <div class="keluhan">
                <h5>Keluhan :</h5>
                <p><?php echo $value['keluhan'];?></p>
            </div>
            <div class="saran">
                <h5>Saran :</h5>
                <p><?php echo $value['saran'];?></p>
            </div>
            <div class="nama">
                <h5>Pengirim :</h5>
                <p><?php echo $value['pengirim'];?></p>
            </div>
            <div class="nama">
                <h5>Status :</h5>
                <p><?php echo $value['status'];?></p>
            </div>
            <?php 
            if($value['status']=="baru"){
                        echo
                        "<a id='tanggap_keluhan' data-toggle='modal' data-target='#tanggap-keluhan' data-id_keluhan='".$value['id_keluhan']."'"." data-user_tanggapan='".$_SESSION['akun_username']."'".">
                            <button style='border-radius:8px;' class='btn btn-secondary btn-xs'><i class='fa fa-edit'></i>Tanggapi</button>
                        </a>";
            }else if($value['status']=="ditanggapi"){
                echo "<a id='tanggap_keluhan' data-toggle='modal' data-target='#tanggap-keluhan' data-id_keluhan='".$value['id_keluhan']."'"." data-user_tanggapan='".$_SESSION['akun_username']."'".">
                <button style='border-radius:8px;' class='btn btn-secondary btn-xs'><i class='fa fa-edit'></i>Ubah tanggapan</button>
            </a>";
            }else if($value['status']=="dibaca"){
               echo "                        <a id='hapus_user' data-toggle='modal' data-target='#hapus-user' data-id_keluhan='".$value['id_keluhan']."'"."data-nama2='".$value['pengirim']."'".">
               <button style='border-radius:8px;' class='btn btn-danger btn-xs'><i class='fa fa-delete'></i>Hapus</button>
           </a>
           </td>";
            }
            ?>
        </div>
    </div>
<?php }?>
</div>
<?php }?>

<?php if($_SESSION['akun_level'] == 'umum'){?>
    <div class="container" style="margin-bottom:30px;">
        <div class="container-fluid" style="background-color:#BBCAF5; border-radius:5px; box-shadow: 0 2px 6px rgba(0, 0, 0, 0.12), 0 1px 4px rgba(0, 0, 0, 0.24);">
            <p>Silakan tuliskan keluhan saran anda. Keluhan dan saran bisa mengenai fasilitas umum, kinerja ketua RT, maupun permasalahan terhadap tetanggan anda!</p>
        </div>
        <form id="form-keluhan" method="POST">
            <div class="row">
                <div class="col-25">
                    <label for="keluhan">Keluhan</label>
                </div>
                <div class="col-75">
                    <textarea id="keluhan" name="keluhan" placeholder="Jelaskan keluhan anda.." style="height:200px"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="saran">Saran</label>
                </div>
                <div class="col-75">
                    <textarea id="saran" name="saran" placeholder="Masukan saran anda.." style="height:200px"></textarea>
                </div>
            </div>
            <div class="row" style="content:align-center;">
            <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
            <!-- <input type="submit" class="btn btn-primary" name="submit" value="Simpan"> -->
            </div>
        </form>
    </div>
<?php }?>

<!-- TANGGAPAN -->
<div  style="width:100%;"class="modal fade" id="tanggap-keluhan" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalScrollableTitle">Form Tanggapan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form id="form_tanggapan" enctype="multipart/form-data">
                <div class="modal-body" id="modal-edit">
                    <label for="tanggapan" style="color:#fff; font-weight:bold;">Tanggapan :</label>
                    <input class="form-control" type="text" name="tanggapan" id="tanggapan" placeholder="Masukan tanggapan anda...">
                    <!-- <br> -->
                    <input type="hidden" id="id_keluhan" name="id_keluhan">
                    <input type="hidden" id="user_tanggapan" name="user_tanggapan">
                    <br/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
                    <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
                </div>
            </form>
        </div>
        </div>
    </div>

   <!-- modal hapus user -->
<div class="modal" id="hapus-user" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <form id="form_hapus_keluhan" enctype="multipart/form-data">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Hapus Keluhan?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="modal-hapus">
            <div class="form-group">
                <p>Yakin hapus data atas nama <span id="nama2"></span>?</p>
                <!-- <input type="text" id="nama" name="nama"> -->
                <input  type="hidden" id="id_keluhan" name="id_keluhan">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
            <input type="submit" class="btn btn-primary" name="submit" value="Hapus">
        </div>
        </div>
    </form>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).on("click", "#tanggap_keluhan", function() {
        var id_keluhan = $(this).data('id_keluhan');
        var user_tanggapan = $(this).data('user_tanggapan');
        $("#modal-edit #id_keluhan").val(id_keluhan);
        $("#modal-edit #user_tanggapan").val(user_tanggapan);
        // $("#modal-edit #tanggapan").val(tanggapan);

    })

    $(document).ready(function(e) {
        $("#form_tanggapan").on("submit", (function(e) {
        e.preventDefault();
        $.ajax({
            url:'input_tanggapan.php',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(msg) {
            $('.table').html(msg);
            }
        });
        window.location="?page=keluhanDanSaran";
        }));
    });

    // HAPUS USER
$(document).on("click", "#hapus_user", function() {
    var id = $(this).data('id_keluhan');
    var nama = $(this).data('nama2');
    $("#modal-hapus #id_keluhan").val(id);
    $("#modal-hapus #nama2").text(nama);

})

$(document).ready(function(e) {
    $("#form_hapus_keluhan").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'hapus_keluhan.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=keluhanDanSaran";
    }));
});
</script>