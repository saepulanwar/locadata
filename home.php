<style>
        div.overflow-auto {
        background-color: white;
        /* width: 31%; */
        height: 400px;
        /* overflow: auto; */
        margin-right: 2%;
        }
        .box{
            background-color:white;
        }
        .side-info{
            background-color:#949494;
            color:#ffff;
            height: 100%;
        }
        iframe {
        width: 90%;
        height: 300px;
        background-color: grey;
        margin-left: 5%;
        }
        .text{
            text-align: justify;
            padding-left: 5%;
            padding-right: 5%;
            padding-top: 5%;
        }
</style>
  </style>
<?php
    ob_start();

    if(!isset($_SESSION['akun_id'])) header("location: login.php");
    include "config.php";

      
      $servername = "localhost";
      $username = "root";
      $password = "";
      $dbname = "locadata";
      
      // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
      // Check connection
      if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
      }
      
      $sql = "SELECT id, judul, isi, tanggal, gambar, penulis FROM artikel ORDER BY tanggal DESC LIMIT 3";
      
      $result = $conn->query($sql);
    ?>
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">
            <h4>Berita Terkini</h4>
    <div class="row">
    <div class="col-md-8">
    <?php
    foreach($result as $key=>$value){
    echo "
    <div style='padding-bottom:20px;' class='box'>
        <p style='text-align:right; padding-right:2px; font-size:10px;'>Tanggal update: ".$value['tanggal']."</p> 
        <h3 style='text-align:center;'>".$value['judul']."</h3>
    <div class=' col-md-12 overflow-auto'>
        <p>".$value['isi']."</p>
        <p style='text-align:right; padding-right:2px; font-size:10px;'>Ditulis oleh: ".$value['penulis']."</p>
    </div>
    <div style='text-align:center;'>";
    if($_SESSION['akun_level'] == 'admin'){
        echo
    "<a id='edit_user' data-toggle='modal' data-target='#edit-user' data-id='".$value['id']."'"." data-judul='".$value['judul']."'"." data-isi='".$value['isi']."'".">
        <button style='border-radius:8px;' class='btn btn-secondary btn-xs'><i class='fa fa-edit'></i>Edit</button>
    </a>
    <a id='hapus_artikel' data-toggle='modal' data-target='#hapus-artikel' data-id='".$value['id']."'"." data-judul='".$value['judul']."'".">
        <button style='border-radius:8px;' class='btn btn-secondary btn-xs'><i class='fa fa-delete'></i>Hapus</button>
    </a>";
    }
    echo
    "</div>
    </div>
    <br>";
    }
    ?>
    </div>
    <div class="col-md-4">
        <div class="side-info">
            <div class="text">
                <h4>About US</h4>
                <p>Website <strong>LOCADATA</strong> adalah sebuah sistem informasi yang berfungsi untuk mengelola data dan informasi bagi RT 03 / RW 01. Melalui website ini Ketua RT dapat melakukan pengelolaan informasi secara lebih efektif</p>
                <p>Alamat : Jl K.J. Damanhuri KM 4</p>
            </div>
            <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.819956352945!2d106.85861071431702!3d-6.912119069569059!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68373e46de3b5b%3A0x58938ea5a81e57d9!2sJl.%20K.H.%20Damanhuri%2C%20Cimahi%2C%20Kec.%20Cicantayan%2C%20Sukabumi%20Regency%2C%20Jawa%20Barat!5e0!3m2!1sen!2sid!4v1588843852284!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
    </div>
    </div>
    <br>

    <!-- EDIT ARTIKEL -->
    <div  style="width:100%;"class="modal fade" id="edit-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalScrollableTitle">Form Edit Artikel</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form id="form_edit_artikel" enctype="multipart/form-data">
                <div class="modal-body" id="modal-edit">
                    <label for="judul" style="color:#fff; font-weight:bold;">Judul :</label>
                    <input class="form-control" type="text" name="judul" id="judul" placeholder="Masukan judul...">
                    <!-- <br> -->
                    <label for="artikel" style="color:#fff; font-weight:bold;">Isi :</label>
                    <textarea name="isi" class="form-control" id="isi">
                    </textarea>
                    <input type="hidden" id="id" name="id">
                    <br/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
                    <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
                </div>
            </form>
        </div>
        </div>
    </div>

    <!-- HAPUS DATA -->
    <div class="modal" id="hapus-artikel" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <form id="form_hapus_artikel" enctype="multipart/form-data">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Artikel?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-hapus">
                <div class="form-group">
                    <p>Yakin hapus artikel dengan judul "<span id="judul"></span>" ?</p>
                    <!-- <input type="text" id="tes"> -->
                    <!-- <input type="text" id="nama" name="nama"> -->
                    <input  type="hidden" id="id" name="id">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
                <input type="submit" class="btn btn-primary" name="submit" value="Hapus">
            </div>
            </div>
        </form>
    </div>
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script type="text/javascript">
    // HAPUS ARTIKEL
    $(document).on("click", "#hapus_artikel", function() {
        var id = $(this).data('id');
        var judul = $(this).data('judul');

        $("#modal-hapus #id").val(id);
        $("#modal-hapus #judul").text(judul);


    })

    $(document).ready(function(e) {
        $("#form_hapus_artikel").on("submit", (function(e) {
        e.preventDefault();
        $.ajax({
            url:'hapus_artikel.php',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(msg) {
            $('.table').html(msg);
            }
        });
        window.location="?page=";
        }));
    });

    // EDIT ARTIKEL
    $(document).on("click", "#edit_user", function() {
        var id = $(this).data('id');
        var judul = $(this).data('judul');
        var isi = $(this).data('isi');
        $("#modal-edit #id").val(id);
        $("#modal-edit #judul").val(judul);
        $("#modal-edit #isi").text(isi);
        

    })

    $(document).ready(function(e) {
        $("#form_edit_artikel").on("submit", (function(e) {
        e.preventDefault();
        $.ajax({
            url:'edit_artikel.php',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(msg) {
            $('.table').html(msg);
            }
        });
        window.location="?page=";
        }));
    });
</script>


    
