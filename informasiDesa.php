<style>
        div.overflow-auto {
        background-color: white;
        /* width: 31%; */
        height: 400px;
        /* overflow: auto; */
        margin-right: 2%;
        }
        .box{
            background-color:white;
        }
        .side-info{
            background-color:#E9EBEE;
            color:black;
            height: 100%;
            border-radius:20px;
        }
        iframe {
        width: 90%;
        height: 300px;
        background-color: grey;
        margin-left: 5%;
        }
        .text{
            text-align: justify;
            padding-left: 5%;
            padding-right: 5%;
            padding-top: 5%;
        }
        .text_judul{
            text-align: justify;
            padding-left: 5%;
            padding-right: 5%;
            padding-top: 5%;
        }
        li:hover{
            background-color:white;
        }
        li{
            list-style:none;
            border-radius:10px;
        }
        .text{
            cursor:pointer;
        }
</style>
  </style>
<?php
    ob_start();

    if(!isset($_SESSION['akun_id'])) header("location: login.php");
    include "config.php";
      
      $servername = "localhost";
      $username = "root";
      $password = "";
      $dbname = "locadata";
      
      // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
      // Check connection
      if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
      }
      
      $sql = "SELECT * FROM informasi_desa ORDER BY tanggal DESC LIMIT 3";
      $result = $conn->query($sql);

      $sql2 = "SELECT * FROM informasi_desa ORDER BY tanggal DESC";
      $result2 = $conn->query($sql2);
    ?>
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">
            <h4>Informasi Terkini</h4>
    <div class="row">
    <div class="col-md-8">
    <?php
    foreach($result as $key=>$value){
    echo "
    <div style='padding-bottom:20px;' class='box'>
        <p style='text-align:right; padding-right:2px; font-size:10px;'>Tanggal update: ".$value['tanggal']."</p> 
        <h3 style='text-align:center;'>".$value['judul']."</h3>
    <div class=' col-md-12 overflow-auto'>
        <p>".$value['isi']."</p>
        <p style='text-align:right; padding-right:2px; font-size:10px;'>Ditulis oleh: ".$value['penulis']."</p>
    </div>
    <div style='text-align:center;'>";
    if($_SESSION['akun_level'] == 'admin'){
        echo
    "<a id='edit_user' data-toggle='modal' data-target='#edit-user' data-id_informasi='".$value['id_informasi']."'"." data-judul='".$value['judul']."'"." data-isi='".$value['isi']."'".">
        <button style='border-radius:8px;' class='btn btn-secondary btn-xs'><i class='fa fa-edit'></i>Edit</button>
    </a>
    <a id='hapus_artikel' data-toggle='modal' data-target='#hapus-artikel' data-id_informasi='".$value['id_informasi']."'"." data-judul='".$value['judul']."'".">
        <button style='border-radius:8px;' class='btn btn-secondary btn-xs'><i class='fa fa-delete'></i>Hapus</button>
    </a>";
    }
    echo
    "</div>
    </div>
    <br>";
    }
    ?>
    </div>
    <div class="col-md-4">
        <div class="side-info">
            <div class="text_judul">
                <p>Daftar Informasi</p>
            </div>
            <div class="text">
            <?php
                foreach($result2 as $key=>$value){
                echo "<a id='informasi_desa' data-toggle='modal' data-target='#informasi-desa' data-id_informasi='".$value['id_informasi']."'"." data-judul='".$value['judul']."'"." data-isi='".$value['isi']."'".">
                <li class='fa-info-circle'>".$value['judul']." "."<small style='color:grey;'>tanggal:"." ".$value['tanggal']."</small></li>
                </a>";
                }
            ?>
            </div>
        </div>
    </div>
    </div>
    <br>
    <!-- <i class='fas fa-info-circle'></i> -->
    <!-- EDIT INFORMASI -->
    <div  style="width:100%;"class="modal fade" id="edit-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalScrollableTitle">Form Edit Artikel</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form id="form_edit_artikel" enctype="multipart/form-data">
                <div class="modal-body" id="modal-edit">
                    <label for="judul" style="color:#fff; font-weight:bold;">Judul :</label>
                    <input class="form-control" type="text" name="judul" id="judul" placeholder="Masukan judul...">
                    
                    <!-- <br> -->
                    <label for="artikel" style="color:#fff; font-weight:bold;">Isi :</label>
                    <textarea name="isi" class="form-control" id="isi">
                    </textarea>
                    <input type="hidden" id="id_informasi" name="id_informasi">

                    
                    <br/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
                    <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
                </div>
            </form>
        </div>
        </div>
    </div>

    <!-- POP UP INFORMASI DESA -->
    <div class="modal fade" id="informasi-desa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalScrollableTitle">Form Edit Artikel</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form id="form_edit_artikel" enctype="multipart/form-data">
                <div class="modal-body" id="modal-info">
                    <h5 style="text-align:center;" name="judul" id="judul"></h5>
                    <span name="isi" id="isi"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
        </div>
    </div>


   

    <!-- HAPUS DATA -->
    <div class="modal" id="hapus-artikel" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <form id="form_hapus_artikel" enctype="multipart/form-data">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Informasi?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-hapus">
                <div class="form-group">
                    <p>Yakin hapus informasi dengan judul "<span id="judul"></span>" ?</p>
                    <!-- <input type="text" id="tes"> -->
                    <!-- <input type="text" id="nama" name="nama"> -->
                    <input  type="hidden" id="id_informasi" name="id_informasi">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
                <input type="submit" class="btn btn-primary" name="submit" value="Hapus">
            </div>
            </div>
        </form>
    </div>
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script type="text/javascript">
    // HAPUS ARTIKEL
    $(document).on("click", "#hapus_artikel", function() {
        var id_informasi = $(this).data('id_informasi');
        var judul = $(this).data('judul');

        $("#modal-hapus #id_informasi").val(id_informasi);
        $("#modal-hapus #judul").text(judul);


    })

    $(document).ready(function(e) {
        $("#form_hapus_artikel").on("submit", (function(e) {
        e.preventDefault();
        $.ajax({
            url:'hapus_informasi.php',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(msg) {
            $('.table').html(msg);
            }
        });
        window.location="?page=informasiDesa";
        }));
    });

    // EDIT ARTIKEL
    $(document).on("click", "#edit_user", function() {
        var id_informasi = $(this).data('id_informasi');
        var judul = $(this).data('judul');
        var isi = $(this).data('isi');
        $("#modal-edit #id_informasi").val(id_informasi);
        $("#modal-edit #judul").val(judul);
        $("#modal-edit #isi").text(isi);
        

    })

    $(document).ready(function(e) {
        $("#form_edit_artikel").on("submit", (function(e) {
        e.preventDefault();
        $.ajax({
            url:'edit_informasi.php',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(msg) {
            $('.table').html(msg);
            }
        });
        window.location="?page=informasiDesa";
        }));
    });

// INFORMASI DESA
$(document).on("click", "#informasi_desa", function() {
        var id_informasi = $(this).data('id_informasi');
        var judul = $(this).data('judul');
        var isi= $(this).data('isi');
        var isi2=isi.replace(/<p>|RT/g,'');
        $("#modal-info #id_informasi").text(id_informasi);
        $("#modal-info #judul").text(judul);
        $("#modal-info #isi").text(isi);
        

    })
</script>


    
