<style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    margin-bottom: 10px;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }
  .table-title{
    padding-top:10px;
  }

  .info a {
    color: #ffff; 
    } /* CSS link color */

</style>
<?php  
    ob_start();

    if(!isset($_SESSION['akun_id'])) header("location: login.php");
    include "config.php";


    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "locadata";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "SELECT * FROM laporan";
    
    $result = $conn->query($sql);
    $result; 
?>


<div class="row">
    <div class="col-md-12">
        <div class="container-fluid" style="background-color: white; border-radius: 10px;">
            <p class="table-title">Data Status</p>
            <table id="mytable">
                <tr>
                    <th>No</th>
                    <!-- <th>NIK Pelapor</th>
                    <th>Nama Pelapor</th> -->
                    <th>NIK</th>
                    <th>Nama</th>
                    <!-- <th>No KK</th> -->
                    <th>Tanggal</th>
                    <th>Status</th>
                    <th>Keterangan</th>
                    <th>Saran</th>
                </tr>
                <?php
                $num = 1;
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                ?>
                <tr>
                    <td><?php echo $num++;?></td>
                    <!-- <td><?php echo $row['nik_pelapor'];?></td>
                    <td><?php echo $row['nama_pelapor'];?></td> -->
                    <td><?php echo $row['nik_terlapor'];?></td>
                    <td><?php echo $row['nama_terlapor'];?></td>
                    <!-- <td><?php echo $row['no_kk'];?></td> -->
                    <td><?php echo $row['tanggal_laporan'];?></td>
                    <td class="status" onclick=myFunction()><?php echo $row['status'];?></td>
                    <td>
                    <?php if ($row['status'] == "sehat"){
                        echo "-";
                    }else{
                        echo $row['keterangan'];
                   }?>
                   </td>
                   <td style="color:<?php if($row['status'] == "sakit"){ echo "red";} ?>">
                   <?php if ($row['status'] == "sehat"){
                        echo "-";
                    }else{
                        echo "Segera lakukan Penanganan";
                   }?>
                   </td>
                </tr>
                <?php
                    }
                } else {
                    echo "0 results";
                }
                // $conn->close();
                ?>
            </table>                        
        </div>
    </div>
</div>

