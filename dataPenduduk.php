<style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    margin-bottom: 10px;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }
  .table-title{
    padding-top:10px;
  }

  .info a {
    color: #ffff; 
    } /* CSS link color */

    .horizontal {
  overflow-x: scroll;
  overflow-y: hidden;
  white-space: nowrap;
  width: 100%;
}

</style>
<?php  
ob_start();

if(!isset($_SESSION['akun_id'])) header("location: login.php");
include "config.php";

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "locadata";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "SELECT * FROM data_penduduk ORDER BY nama ASC";
    
    $result = $conn->query($sql);
    // foreach($result as $key=>$value){
    //     echo $value['nama'];
    // }
?>

<div class="row horizontal">
    <div class="col-md-12">
    <!-- <a id='tambah_data' data-toggle='modal' data-target='#tambah-data'><button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah Data</button></a> -->
        <div class="container-fluid" style="background-color: white; border-radius: 10px; width: 170%">
            <span class="row">
                <p class="table-title">Data Penduduk</p>
                
                <?php if($_SESSION['akun_level'] == 'admin'){?>
                    <input style=" margin:10px; float:left;" type="text" id="myInput" onkeyup="cariNama()" placeholder="Cari nama..." title="Type in a name">
                    <input style=" margin:10px; float:right;" type="text" id="kk" onkeyup="cariKK()" placeholder="Cari KK..." title="Tulis KK">
                <?php } else{?>
                    <input style=" margin:10px; float:left;" type="text" id="myInput2" onkeyup="cariNama2()" placeholder="Cari nama..." title="Type in a name">
                <?php }?>
            </span>
            <span>*Tekan shift lalu putar tombol <i>scroll</i> pada mouse untuk menggeser tabel ke kanan dan kiri</span>
            <table class="table" id="myTable">
                <thead>
                    <tr>
                    <th scope="col">No</th>
                    <th scope="col">NIK</th>
                    <?php if($_SESSION['akun_level'] == 'admin'){?>
                        <th scope="col">No KK</th>
                    <?php } ?>
                    <th scope="col">Nama</th>
                    <th scope="col">Tanggal Lahir</th>
                    <th scope="col">Tempat Lahir</th>
                    <th scope="col">Pekerjaan</th>
                    <th scope="col">Pendidikan</th>
                    <th scope="col">Agama</th>
                    <th>Umur (tahun)</th>
                    <th scope="col">Status Kawin</th>
                    <?php if($_SESSION['akun_level'] == 'admin'){?>
                        <th scope="col">*</th>
                    <?php }?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $num = 1;
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                    ?>
                    <tr>
                        <th scope="row"><?php echo $num++;?></th>
                        <td><?php echo $row['nik'];?></td>
                        <?php if($_SESSION['akun_level'] == 'admin'){?>
                            <td><?php echo $row['no_kk'];?></td>
                        <?php }?>
                        <td><?php echo $row['nama'];?></td>
                        <td><?php echo $row['tgl_lahir'];?></td>
                        <td><?php echo $row['tempat_lahir'];?></td>
                        <td><?php echo $row['pekerjaan'];?></td>
                        <td><?php echo $row['pendidikan'];?></td>
                        <td><?php echo $row['agama'];?></td>
                        <?php $dari = new DateTime($row['tgl_lahir']);
                                $ke   = new DateTime('today');
                                $umur=$dari->diff($ke)->y;?>
                        <td <?php if($umur>45){echo "style='color:red'";}?>><?php $from = new DateTime($row['tgl_lahir']);
                                $to   = new DateTime('today');
                                echo $from->diff($to)->y;?></td>
                        <td><?php echo $row['status_kawin'];?></td>
                        <?php 
                            if($_SESSION['akun_level']=='admin'){
                            echo 
                            "<td align='center'>
                            <a id='edit_data' data-toggle='modal' data-target='#edit-data' data-id_data='".$row['id_data']."'"."data-nama='".$row['nama']."'"."data-nik='".$row['nik']."'"."data-nokk='".$row['no_kk']."'"."data-tgl_lahir='".$row['tgl_lahir']."'"."data-tempat_lahir='".$row['tempat_lahir']."'"."data-pekerjaan='".$row['pekerjaan']."'"."data-pendidikan='".$row['pendidikan']."'"."data-agama='".$row['agama']."'"."data-status_kawin='".$row['status_kawin']."'".">
                                <button style='border-radius:8px;' class='btn btn-primary btn-xs'><i class='fa fa-edit'></i>Edit</button>
                            </a>
                            <a id='hapus_data' data-toggle='modal' data-target='#hapus-data' data-id_data='".$row['id_data']."'"."data-nama='".$row['nama']."'"."data-nik='".$row['nik']."'"."data-no_kk='".$row['no_kk']."'"."data-tgl_lahir='".$row['tgl_lahir']."'".">
                                <button style='border-radius:8px;' class='btn btn-danger btn-xs'><i class='fa fa-delete'></i>Hapus</button>
                            </a>
                            </td>";
                        }
                        ?>
                    </tr>
                    <?php
                        }
                    } else {
                        echo "0 results";
                    }
                    // $conn->close();
                    ?>
                </tbody>
            </table>                
        </div>
    </div>
</div>

<!-- Modal pop up edit data -->
<div class="modal fade" id="edit-data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Form Perubahan Data Penduduk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form_edit_data" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
        
            <div class="form-group">
                <label for="nik">NIK</label>
                <input type="text" name="nik" class="form-control" id="nik" required>
                <input  type="hidden" id="id_data" name="id_data">
            </div>
            <div class="form-group">
                <label for="nokk">No KK</label>
                <input type="text" name="nokk" class="form-control" id="nokk" required>
            </div>
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" name="nama" class="form-control" id="nama" required>
            </div>
            <div class="form-group">
                <label for="tgl_lahir">Tanggal Lahir</label>
                <input type="text" name="tgl_lahir" class="form-control" id="tgl_lahir" required>
            </div>
            <div class="form-group">
                <label for="tempat_lahir">Tempat Lahir</label>
                <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir" required>
            </div>
            <div class="form-group">
                <label for="pekerjaan">Pekerjaan</label>
                <input type="text" name="pekerjaan" class="form-control" id="pekerjaan" required>
            </div>
            <div class="form-group">
                <label for="pendidikan">Pendidikan</label>
                <input type="text" name="pendidikan" class="form-control" id="pendidikan" required>
            </div>
            <div class="form-group">
                <label for="agama">Agama</label>
                <input type="text" name="agama" class="form-control" id="agama" required>
            </div>
            <div class="form-group">
                <label for="status_kawin">Status Kawin</label>
                <input type="text" name="status_kawin" class="form-control" id="status_kawin" required>
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
        </div>
        </form>
    </div>
    </div>
</div>

<!-- MODAL HAPUS DATA PENDUDUK -->

<div class="modal fade" id="hapus-data" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Anda Yakin?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form_hapus_data" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
        
            <div class="form-group">
                <label for="nip">Yakin anda akan menghapus data penduduk dengan nama <p name="nama" id="nama"></p> </label>
                <!-- <label for="nip">NIP : <p name="nip" id="nip"></p> </label> -->
                <input  type="hidden" id="id_data" name="id_data">
                <!-- <input type="text" name="nama" class="form-control" id="nama" required> -->
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Yakin">
        </div>
        </form>
    </div>
    </div>
</div>

 <!-- MODAL TAMBAH DATA -->

<div class="modal fade" id="tambah-data" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Form Tambah User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form-tambah" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
        <div class="form-group">
                <label for="nik">NIK</label>
                <input type="number" id="nik" name="nik" class="form-control" required>
                <div id="feed"></div>
            </div>
            <div class="form-group">
                <label for="no_kk">No KK</label>
                <input type="number" id="no_kk" name="no_kk" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" id="nama" name="nama" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="jenis_kelamin">Jenis Kelamin</label>
                <select class="dropdown form-control" id = "jenis_kelamin" name="jenis_kelamin">
                    <option value = "laki-laki">Laki-Laki</option>
                    <option value = "perempuan">Perempuan</option>
                </select>
            </div>
            <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir</label>
                <input type="date" name="tanggal_lahir" class="form-control" id="tanggal_lahir" required>
            </div>
            <!-- <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir</label>
                <select name="tanggal" id="tanggal">
                    <option value="">Pilih tanggal</option>
                    <?php for($d = 1; $d <=31; $d++){?>
                    <option value="<?php echo $d; ?>"><?php echo $d;?></option>
                    <?php } ?>
                </select>
                <select name="bulan" id="bulan">
                    <option value="">Pilih bulan</option>
                    <?php for($m = 1; $m <=12; $m++){?>
                    <?php $nama_bulan= array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember')?>
                    <option value="<?php echo $m; ?>"><?php echo $nama_bulan[$m-1];?></option>
                    <?php } ?>
                </select>
                <select name="tahun" id="tahun">
                    <option value="">Pilih tahun</option>
                    <?php for($y = date('Y'); $y >=1900; $y--){?>
                    <option value="<?php echo $y; ?>"><?php echo $y;?></option>
                    <?php } ?>
                </select>
            </div> -->
            <div class="form-group">
                <label for="tempat_lahir">Tempat Lahir</label>
                <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir" required>
            </div>
            <div class="form-group">
                <label for="pekerjaan">Pekerjaan</label>
                <input type="text" name="pekerjaan" class="form-control" id="pekerjaan" required>
            </div>
            <div class="form-group">
                <label for="dd">Pendidikan</label>
                <select class="dropdown form-control" id = "pendidikan" name="pendidikan">
                    <option value="" disabled>Pilih Pendidikan terakhir</option>
                    <option value = "Tidak Sekolah">Tidak Sekolah</option>
                    <option value = "SD">SD</option>
                    <option value = "SMP">SMP</option>
                    <option value = "SMA">SMA</option>
                    <option value = "Strata 1">Strata 1</option>
                    <option value = "Strata 2">Strata 2</option>
                </select>
            </div>
            <div class="form-group">
                <label for="agama">Agama</label>
                <input type="text" name="agama" class="form-control" id="agama" required>
            </div>
            <div class="form-group">
                <label for="status_kawin">Status Kawin</label>
                <select class="dropdown form-control" id = "status_kawin" name="status_kawin">
                    <option value = "kawin">kawin</option>
                    <option value = "belum kawin">belum kawin</option>
                    <option value = "cerai">cerai</option>
                    <option value = "cerai mati">cerai mati</option>
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
            <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
        </div>
        </form>
    </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script type="text/javascript">
// EDIT DATA PENDUDUK
$(document).on("click", "#edit_data", function() {
    var id_data = $(this).data('id_data');
    var nama = $(this).data('nama');
    var nik = $(this).data('nik');
    var nokk = $(this).data('nokk');
    var tempat_lahir = $(this).data('tempat_lahir');
    var tgl_lahir = $(this).data('tgl_lahir');
    var pekerjaan = $(this).data('pekerjaan');
    var pendidikan = $(this).data('pendidikan');
    var agama = $(this).data('agama');
    var status_kawin = $(this).data('status_kawin');    
    $("#modal-edit #id_data").val(id_data);
    $("#modal-edit #nama").val(nama);
    $("#modal-edit #nik").val(nik);
    $("#modal-edit #nokk").val(nokk);
    $("#modal-edit #tempat_lahir").val(tempat_lahir);
    $("#modal-edit #tgl_lahir").val(tgl_lahir);
    $("#modal-edit #pekerjaan").val(pekerjaan);
    $("#modal-edit #pendidikan").val(pendidikan);
    $("#modal-edit #agama").val(agama);    
    $("#modal-edit #status_kawin").val(status_kawin);  
})
$(document).ready(function(e) {
    $("#form_edit_data").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'edit_data_penduduk.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=dataPenduduk";
    }));
});

// HAPUS DATA
$(document).on("click", "#hapus_data", function() {

    var nama = $(this).data('nama');
    var id = $(this).data('id_data');
    $("#modal-edit #nama").text(nama);
    $("#modal-edit #id_data").val(id);
})

// $(document).on("click", "#hapus_data", function() {
//     var id = $(this).data('id_data');
//     // var nama = $(this).data('nama');
//     var nama = 'tes';

//     // var username = $(this).data('username');
//     $("#modal-hapus #id_data").val(id);
//     $("#modal-hapus #nama").val(nama);
  
//     // $("#modal-hapus #username").val(username);

// })

$(document).ready(function(e) {
    $("#form_hapus_data").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'hapus_data_penduduk.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=dataPenduduk";
    }));
});

// TAMBAH DATA PENDUDUK
$(document).ready(function(e) {
    $("#form-tambah").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'tambah_data.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=dataPenduduk";
    }));
    
});

function cariNama() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
function cariNama2() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}

function cariKK() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("kk");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}

$(document).ready(function(){
    $('#feed').load('cekData.php').show();
    $('#nik').keyup(function(){
        $.post('cekData.php', {nik: form.nik.value}, 
        function(result){
            $('#feed').html(result).show();
        });
    });
});
</script>